type Name = {name: string};
export type ByName<T extends Name> = {[name: string]: T};

export function byName<T extends Name>(arr: T[]): ByName<T> {
  let result: ByName<T> = {}
  for (let item of arr)
    // FIXME: Check for duplicates.
    result[item.name] = item
  return result
}

