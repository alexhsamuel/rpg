import type { Character } from './character'

export const CHARACTER: Character =  {
  characterId: "eucomis",
  name: "Eucomis Bonarbor",
  class: "druid",
  level: 1,
  alignment: "neutral",
  abilityScores: {
    strength: 13,
    intelligence: 13,
    wisdom: 15,
    dexterity: 11,
    constitution: 6,
    charisma: 11,
  },
  hitPoints: {
    maximum: 10,
    current: 5,
  },
  languages: {
    literate: true,
    known: [
      "Common",
      "Druidic",
      "Pixie",
    ],
  },
  inventory: [
    {item: "lantern", quantity: 1},
    {item: "flask of oil", quantity: 5},
    // {item: "pole", quantity: 1},
    // {item: "rope", quantity: 1},
    // {item: "mallet", quantity: 1},
    // {item: "stake", quantity: 6},
    // {item: "interbox", quantity: 1},
    // {item: "mistletoe", quantity: 1},
    // {item: "waterskin", quantity: 1},
    // {item: "rations", quantity: 7},
    {item: "leather armor", quantity: 1},
    // {item: "sling", quantity: 1},
  ],
  coins: {
    platinum: 0,
    gold: 17,
    electrum: 0,
    silver: 0,
    copper: 0,
  },
}

