import { writable } from 'svelte/store'
import * as sample from './sample'
import type { CharacterAction } from './character'
import { applyCharacterAction } from './character'

export const character = writable(sample.CHARACTER)

export function apply(action: CharacterAction) {
  character.update((c) => applyCharacterAction(action, c))
}
