import type { Item } from './items'
import { STANDARD_ITEMS } from './items'
import type { Coins } from './money'

export type HitPoints = {
    current: number,
    maximum: number,
}

export type HealAction = {type: 'heal', healing: number | 'max'}
export type DamageAction = {type: 'damage', damage: number}
export type CharacterAction = DamageAction | HealAction

export function applyCharacterAction(action: CharacterAction, character: Character): Character {
  switch (action.type) {
    case 'heal': {
      let healing = (action as HealAction).healing
      let { current, maximum } = character.hitPoints
      current = healing === 'max' ? maximum : Math.min(current + healing, maximum)
      return {...character, hitPoints: {...character.hitPoints, current}}
    }

    case 'damage': {
      let damage = (action as DamageAction).damage
      let { current } = character.hitPoints
      current = Math.max(current - damage, 0)
      return {...character, hitPoints: {...character.hitPoints, current}}
    }
  }
}

//------------------------------------------------------------------------------

type AbilityScores = {
  strength: number,
  intelligence: number,
  wisdom: number,
  dexterity: number,
  constitution: number,
  charisma: number,
}

type AbilityScoreNames = keyof(AbilityScores)

type InventoryItem = {
  item: string | Item,
  quantity: number,
}

export type Character = {
  characterId: string,
  name: string,
  class: string,
  level: number,  // FIXME
  alignment: string,  // FIXME
  abilityScores: AbilityScores,
  hitPoints: HitPoints,
  languages: any,  // FIXME
  inventory: InventoryItem[],
  coins: Coins,
}

//------------------------------------------------------------------------------

type AbilityScoresEx = AbilityScores & {
  meleeModifier: number,
  openDoors: number,
  magicSaveModifier: number,
  armorClassModifier: number,
  missileModifier: number,
  initiativeModifier: number,
  hitPointModifier: number,
  npcReactionModifier: number,
  maxRetainers: number,
  retainerLoyalty: number,
  // FIXME: prime requisite modifier
}

/// Returns a function to look up a modifier by ability score.  `number` is a 
/// 7-element array with values for ability scores 3, 4-5, 6-8, 9-12, 13-15,
/// 16-17, and 18.
function abilityScoreLookup(ability: AbilityScoreNames, table: number[]): (abilityScores: AbilityScores) => number {
  return abilityScores => {
    const val = abilityScores[ability]
    if (val < 3 || 18 < val)
      throw new Error('invalid ability score')
    return (
      val == 3 ? table[0]
      : val < 6 ? table[1]
      : val < 9 ? table[2]
      : val < 13 ? table[3]
      : val < 16 ? table[4]
      : val < 18 ? table[5]
      : table[6]
    )
  }
}

const STANDARD_MODIFIERS = [-3, -2, -1, 0, 1, 2, 3]
const MELEE_MODIFIER = abilityScoreLookup('strength', STANDARD_MODIFIERS)
const OPEN_DOORS = abilityScoreLookup('strength', [1, 1, 1, 2, 3, 4, 5])
const MAGIC_SAVE_MODIFIER = abilityScoreLookup('intelligence', STANDARD_MODIFIERS)
const ARMOR_CLASS_MODIFIER = abilityScoreLookup('dexterity', STANDARD_MODIFIERS)
const MISSILE_MODIFIER = abilityScoreLookup('dexterity', STANDARD_MODIFIERS)
const INITIATIVE_MODIFIER = abilityScoreLookup('dexterity', [-2, -1, -1, 0, 1, 1, 2])
const HIT_POINT_MODIFIER = abilityScoreLookup('constitution', STANDARD_MODIFIERS)
const NPC_REACTION_MODIFIER = abilityScoreLookup('charisma', [-2, -1, -1, 0, 1, 1, 2])
const MAX_RETAINTERS = abilityScoreLookup('charisma', [1, 2, 3, 4, 5, 6, 7])
const REtAINER_LOYALTY = abilityScoreLookup('charisma', [4, 5, 6, 7, 8, 9, 10])


function expandAbilityScores(abilityScores: AbilityScores): AbilityScoresEx {
  return {
    ...abilityScores,
    meleeModifier: MELEE_MODIFIER(abilityScores),
    openDoors: OPEN_DOORS(abilityScores),
    magicSaveModifier: MAGIC_SAVE_MODIFIER(abilityScores),
    armorClassModifier: ARMOR_CLASS_MODIFIER(abilityScores),
    missileModifier: MISSILE_MODIFIER(abilityScores),
    initiativeModifier: INITIATIVE_MODIFIER(abilityScores),
    hitPointModifier: HIT_POINT_MODIFIER(abilityScores),
    npcReactionModifier: NPC_REACTION_MODIFIER(abilityScores),
    maxRetainers: MAX_RETAINTERS(abilityScores),
    retainerLoyalty: REtAINER_LOYALTY(abilityScores),
  }
}

type InventoryItemEx = InventoryItem & {
  item: Item,
}

type CharacterEx = Character & {
  inventory: InventoryItemEx[],
}

function expandInventory(inventory: InventoryItem[]): InventoryItemEx[] {
  return inventory.map(
    i => {
      if (typeof(i.item) === 'string') {
        let item = STANDARD_ITEMS[i.item]
        if (item === undefined) {
          console.log('unknown item', i.item)
          item = {name: i.item, cost: undefined}
        }
        return {...i, item}
      }
      return i as InventoryItemEx
    }
  )
}

export function expandCharacter(character: Character): CharacterEx {
  return {
    ...character,
    abilityScores: expandAbilityScores(character.abilityScores),
    inventory: expandInventory(character.inventory),
  }
}

