export const COIN_TYPES = ['platinum', 'gold', 'electrum', 'silver', 'copper'] as const
type CoinType = typeof COIN_TYPES[number]

export type Coins = {
  [coinType in CoinType]: number
}

type Value = number  // value in gp; use 0.01 for cp 
type _Cost = {value: Value, quantity: number} 
export type Cost = _Cost | number

function normalizeCost(cost: Cost): _Cost {
  return typeof(cost) === "number" ? {value: cost, quantity: 1} : cost
}
