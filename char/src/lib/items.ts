import type { ByName } from './lib'
import { byName } from './lib'
import type { Cost } from './money'

type Dice = string;  // for now

export type Item = {
  name: string,
  cost?: Cost,
}

type Gear = Item;

const STANDARD_GEAR: ByName<Gear> = byName([
  {
    name: "backback",
    cost: 5,
  },
  {
    name: "iron spike",
    cost: {value: 1, quantity: 12},
  },
  {
    name: "lantern",
    cost: 10,
  },
  {
    name: "flask of oil",
    cost: 2,
  },
  {
    name: "standard rations",
    cost: {value: 5, quantity: 7},
  }
]);

type WeaponQuality = "melee" | "missile" | "two-handed" | "slow" | "blunt" | "reload" | "brace" | "charge" | "splash";

type MissileRange = {
  point_blank: number,
  short: number,
  medium: number,
  long: number,
}

type Weapon = {
  name: string,
  cost: Cost,
  weight: number,
  damage: Dice,
  qualities: WeaponQuality[],
  missileRange?: MissileRange,
}

const STANDARD_WEAPONS: ByName<Weapon> = byName([
  {
    name: "battle axe",
    cost: 7,
    weight: 50,
    damage: "1d8",
    qualities: ["melee", "slow", "two-handed"],
  },
  {
    name: "club",
    cost: 3,
    weight: 50,
    damage: "1d4",
    qualities: ["blunt", "melee"],
  },
  {
    name: "crossbow",
    cost: 30,
    weight: 50,
    damage: "1d6",
    qualities: ["missile", "reload", "slow", "two-handed"],
    missileRange: {point_blank: 5, short: 80, medium: 160, long: 240},
  }
]);

type ArmorQuality = "armor" | "shield";

type Armor = {
  name: string,
  armor_class_adj: number,
  cost: number,
  weight: number,
  type: "suit" | "shield",
};

const STANDARD_ARMORS: ByName<Armor> = byName([
  {
    name: "leather armor",
    armor_class_adj: 2,
    cost: 20,
    weight: 200,
    type: "suit",
  },
  {
    name: "chainmail",
    armor_class_adj: 4,
    cost: 40,
    weight: 400,
    type: "suit",
  },
  {
    name: "plate mail",
    armor_class_adj: 6,
    cost: 60,
    weight: 500,
    type: "suit",
  },
  {
    name: "shield",
    armor_class_adj: 1,
    cost: 10,
    weight: 100,
    type: "shield",
  },
]);

export const STANDARD_ITEMS: ByName<Gear> = {...STANDARD_GEAR, ...STANDARD_ARMORS, ...STANDARD_WEAPONS};

