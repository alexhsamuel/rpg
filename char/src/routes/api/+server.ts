import { CHARACTER } from '$lib/sample'
import { json } from '@sveltejs/kit'

export function GET({ url }) {
  console.log('GET', url)
  return json(CHARACTER)
}
