export async function load({ params, fetch }) {
  const character = await (await fetch('/api')).json()
  let data = {
    character: character,
  }
  return data
}
