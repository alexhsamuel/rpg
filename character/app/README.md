## Developing

To run the combined 
```bash
npm run dev
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.

- See: https://noam.hashnode.dev/using-vite-to-serve-and-hot-reload-react-app-express-api-together

