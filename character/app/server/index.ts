import express from 'express'
import { applyCharacterAction } from '../src/lib/game/ose/character'
import { Store } from './store'

export const app = express()
app.use(express.json())

const store = new Store('/home/alex/dev/rpg/character/store')  // FIXME
await store.mkdirs()

app.get('/api/ping', (_, res) => res.json({'data': 'pong'}))

app.get('/api/characters/:name', async (req, res) => {
  const after = req.query.after as string | undefined
  const [version, character] = await store.loadCharacter(req.params.name, after)
  res.json({ version, character })
})

app.put('/api/characters/:name', async (req, res) => {
  const name = req.params.name
  const version = req.query.version
  const action = req.body

  // Load the character.
  const [oldVersion, oldCharacter] = await store.loadCharacter(name)
  if (version !== undefined && version !== oldVersion) {
    // Version conflict.
    res.status(409).end()
    return
  }

  console.log('character action', name, action, version)

  // Apply the action.
  const newCharacter = applyCharacterAction(action, oldCharacter)
  // Store the modified character.
  const newVersion = await store.storeCharacter(name, oldVersion, newCharacter)

  // Return the new version.
  res.json({ version: newVersion, character: newCharacter })
})

