import * as fs from 'node:fs/promises'
import { join } from 'node:path'
import { Mutex } from 'async-mutex'
import { Channel } from 'async-channel'
import type { Character } from '../src/lib/game/ose/character'

// ISO datetime representing the version of an object.
type Version = string

async function maybeMkdir(path: string) {
  try {
    await fs.mkdir(path)
  }
  catch (err) {
    if (err.code !== 'EEXIST')
      throw err
  }
}

async function getVersion(fh: fs.FileHandle): Promise<Version> {
  return (await fh.stat()).mtime.toISOString() 
}

const BUCKETS = ['characters'] as const
type Bucket = typeof BUCKETS[number]

export class Store {
  path: string
  lock: Mutex
  waiting: {
    [bucket in Bucket]?: {
      [key: string]: [
        Version | 'next',
        Channel<any>
      ][]
    }
  }

  constructor(path: string) {
    this.path = path
    this.lock = new Mutex()
    this.waiting = {}
  }

  async mkdirs() {
    await maybeMkdir(this.path)
    for (let bucket of BUCKETS)
      await maybeMkdir(join(this.path, bucket))
  }

  async load<T>(bucket: Bucket, key: string, after: 'next' | Version | undefined = undefined): Promise<[Version, T]> {
    const path = join(this.path, bucket, key + '.json')
    // FIXME: Make sure we don't leak a lock!
    let release = await this.lock.acquire()
    const fh = await fs.open(path, 'r')
    // Check the version of the file.
    const version = await getVersion(fh)
    if (after === undefined || (after !== 'next' && after < version)) {
      // New enough; just read it.
      const json = await fh.readFile('utf8')
      fh.close()
      release()
      const obj = JSON.parse(json)
      return [version, obj] as [Version, T]
    }

    else {
      // Too old.
      fh.close()
    
      // Register a channel to get notified when the value changes.
      const channel = new Channel<any>()
      const arr = (this.waiting[bucket] ??= {})[key] ??= []
      arr.push([after, channel])

      // Return an async fn that reads the updated value from the channel.
      release()
      return (await channel) as [Version, T]
    }
  }

  async store<T>(bucket: Bucket, key: string, lastVersion: Version | null, obj: T): Promise<Version> {
    const path = join(this.path, bucket, key + '.json')
    const json = JSON.stringify(obj, undefined, 2)
    return await this.lock.runExclusive(async () => {
      let version: Version
      let fh: fs.FileHandle
      if (lastVersion === null)
        // New file.
        fh = await fs.open(path, 'wx')
      else {
        // Existing file.
        // FIXME: Handle file not found.
        fh = await fs.open(path, 'r+')
        // Before writing, make sure its timestamp matches the version.
        const version = await getVersion(fh)
        if (version !== lastVersion)
          // FIXME: Custom error type. 
          throw new Error(`inconsistent version: expected ${lastVersion} got ${version}`)
        await fh.truncate(0)
      } 
      try {
        await fh.writeFile(json, 'utf8')
      }
      finally {
        fh.close()
      }

      version = (await fs.stat(path)).mtime.toISOString()

      // Send the new object to all channels waiting on this bucket/key.
      const waitingForBucket = this.waiting[bucket] ??= {}
      const waitingForKey = waitingForBucket[key]
      if (waitingForKey) {
        const newWaitingForKey = waitingForKey.filter(
          ([after, channel]) => {
            if (after === 'next' || after < version) {
              // Ready to notify.  Send the new version, then remove this waiting channel.
              channel.push([version, obj])
              return false
            }
            else
              // Not ready yet.
              return true
          }
        )
        if (newWaitingForKey)
          waitingForBucket[key] = newWaitingForKey
        else
          delete waitingForBucket[key]
      }
    
      return version
    })
  }

  async loadCharacter(key: string, after: Version | undefined = undefined): Promise<[Version, Character]> {
    return this.load<Character>('characters', key, after)
  }

  async storeCharacter(key: string, lastVersion: Version | null, character: Character): Promise<Version> {
    return this.store('characters', key, lastVersion, character)
  }
}
