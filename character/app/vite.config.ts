import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vitest/config';
import express from './server/express-plugin'

export default defineConfig({
	plugins: [sveltekit(), express('server')],

	test: {
		include: ['src/**/*.{test,spec}.{js,ts}']
	},

	css: {
		preprocessorOptions: {
			scss: {
				additionalData: '@use "src/variables.scss" as *;'
			}
		}
	},

	server: {
		host: true
	}		
});
