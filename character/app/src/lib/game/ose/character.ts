import type { CoinType, Coins } from './coins'
import { addCoins } from './coins'
import type { Inventory, EquipLocation, Equipped, InventoryItem } from './inventory'
import { equip, unequip, inventoryAdd, inventoryIncrease, setLoot } from './inventory'
import type { AbilityScores } from './abilityScores'

// FIXME: Elsewhere.
export const CLASSES = [
  'acrobat',
  'assassin',
  'barbarian',
  'bard',
  'cleric',
  'druid',
  'fighter',
  'illusionist',
  'knight',
  'magic-user',
  'paladin',
  'ranger',
  'thief',
] as const
export type Class = typeof CLASSES[number]

export type HitPoints = {
    current: number,
    maximum: number,
}

//------------------------------------------------------------------------------

type Portrait = undefined | { url: string }

export type Character = {
  characterId: string
  name: string
  portrait: Portrait
  class: Class,
  level: number  // FIXME
  alignment: string  // FIXME
  abilityScores: AbilityScores
  hitPoints: HitPoints
  languages: any  // FIXME
  inventory: Inventory
  coins: Coins
  equipped: Equipped,
}

//------------------------------------------------------------------------------

function heal(character: Character, healing: number | 'max'): Character {
  let {current, maximum} = character.hitPoints
  console.log(current, maximum, healing)
  current = healing === 'max' ? maximum : Math.min(current + healing, maximum)
  console.log('new current', current)
  return {...character, hitPoints: {...character.hitPoints, current}}
}

export type HealAction = {type: 'heal', healing: number | 'max'}

function damage(character: Character, damage: number): Character {
  let {current} = character.hitPoints
  current = Math.max(current - damage, 0)
  return {...character, hitPoints: {...character.hitPoints, current}}
}

export type AddCoinsAction = {type: 'addCoins', coinType: CoinType, value: number}
export type DamageAction = {type: 'damage', damage: number}
export type EquipAction = {type: 'equip', itemName: string, location: EquipLocation}
export type InventoryAdd = {type: 'inventoryAdd', item: InventoryItem}
export type InventoryIncrease = {type: 'inventoryIncrease', itemName: string, quantity: number}
export type SetLoot = {type: 'setLoot', index: number, loot: boolean}
export type UnequipAction = {'type': 'unequip', location: EquipLocation}
export type CharacterAction = (
  AddCoinsAction 
  | DamageAction
  | EquipAction
  | HealAction
  | InventoryAdd
  | InventoryIncrease
  | SetLoot
  | UnequipAction
)

export function applyCharacterAction(action: CharacterAction, character: Character): Character {
  switch (action.type) {
    case 'addCoins':
      return addCoins(character, action.coinType, action.value)
    case 'damage': 
      return damage(character, action.damage)
    case 'equip':
      return equip(character, action.itemName, action.location)
    case 'heal':
      return heal(character, action.healing)
    case 'inventoryAdd':
      return inventoryAdd(character, action.item)
    case 'inventoryIncrease':
      return inventoryIncrease(character, action.itemName, action.quantity)
    case 'setLoot':
      return setLoot(character, action.index, action.loot)
    case 'unequip':
      return unequip(character, action.location)
  }
}

