// Movement and encumbrance

import type { Character } from "./character"
import { getInventoryDetails } from "./inventory"

// Carried weight in coins.  One coin = 0.1 lb.
type DetailedEncumbrance = number

const SUM = (t: number, x: number) => t + x

export function getDetailedEncumbrance(character: Character): DetailedEncumbrance {
  const gearEncumbrance = 80
  const itemEncumbrance = getInventoryDetails(character.inventory)
    .map(({item, quantity}) => quantity * (item.weight || 0))
    .reduce(SUM)
  const coinEncumbrance = Object.values(character.coins).reduce(SUM)
  return gearEncumbrance + itemEncumbrance + coinEncumbrance
}

type Movement = {
  // Nominal base movement rate.
  base: number,  
  // Encounter rate in feet / round.
  encounter: number,
  // Overland rate in miles / day.
  overland: number,
  // Exploration rate in feet / turn.
  exploration: number,
}

export function getMovement(enc: DetailedEncumbrance): Movement {
  const base = enc <= 400 ? 120 : enc <= 600 ? 90 : enc <= 800 ? 60 : enc <= 1600 ? 30 : 0
  return {
    base,
    encounter: base / 3,
    overland: base / 5,
    exploration: base,
  }
}

