import type { Character } from './character'

export const SAVING_THROW_KINDS = ['death', 'wands', 'paralysis', 'breathAttacks', 'spells'] as const
export type SavingThrowKind = typeof SAVING_THROW_KINDS[number]
export type SavingThrows = {[kind in SavingThrowKind]: number}

const SAVING_THROWS_TABLE = (
  [
    [
      ['acrobat', 'assassin', 'bard', 'thief'],
      [
        [ 4, [13, 14, 13, 16, 15]],
        [ 8, [12, 13, 11, 14, 13]],
        [12, [10, 11,  9, 12, 10]],
        [14, [ 8,  9,  7, 10,  8]],
      ]
    ],
    [
      ['barbarian'],
      [
        [ 3, [10, 13, 12, 15, 16]],
        [ 6, [ 8, 11, 10, 13, 13]],
        [ 9, [ 6,  9,  8, 10, 10]],
        [12, [ 4,  7,  6,  8,  7]],
        [14, [ 3,  5,  4,  5,  5]],
      ]
    ],
    [
      ['cleric', 'druid'],
      [
        [ 4, [11, 12, 14, 16, 15]],
        [ 8, [ 9, 10, 12, 14, 12]],
        [12, [ 6,  7,  9, 11,  9]],
        [14, [ 3,  5,  7,  8,  7]],
      ]
    ],
    [
      ['fighter', 'knight', 'ranger'],
      [
        [ 3, [12, 13, 14, 15, 16]],
        [ 6, [10, 11, 12, 13, 14]],
        [ 9, [ 8,  9, 10, 10, 12]],
        [12, [ 6,  7,  8,  8, 10]],
        [14, [ 4,  5,  6,  5,  8]],
      ]
    ],
    [
      ['illusionist', 'magic-user'],
      [
        [ 5, [13, 14, 13, 16, 15]],
        [10, [11, 12, 11, 14, 12]],
        [14, [ 8,  9,  8, 11,  8]],
      ]
    ],
    [
      ['paladin'],
      [
        [ 3, [10, 11, 12, 13, 14]],
        [ 6, [ 8,  9, 10, 11, 12]],
        [ 9, [ 6,  7,  8,  8, 10]],
        [12, [ 4,  5,  6,  6,  8]],
        [14, [ 2,  3,  4,  3,  6]],
      ]
    ]
  ] as [string[], [number, number[]][]][]
)

export function getSavingThrows(character: Character): SavingThrows {
  let savingThrows: SavingThrows | undefined = undefined
  outer:
  for (let [classes, rows] of SAVING_THROWS_TABLE)
    if (classes.includes(character.class))
      for (let [maxLevel, entries] of rows)
        if (character.level <= maxLevel){
          let [death, wands, paralysis, breathAttacks, spells] = entries
          savingThrows = {death, wands, paralysis, breathAttacks, spells}
          break outer
        }
  if (savingThrows === undefined)
    throw new Error(`no saving throws for level ${character.level} ${character.class}`)

  return savingThrows
}
