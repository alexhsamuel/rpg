import type { ArmorItem, ShieldItem } from './items'
import { isArmor, isShield } from './items'
import type { Character } from './character'
import { getAbilityScoreDetails } from './abilityScores'
import { getEquippedItem } from './inventory'

export type ACDetails = {
  // AC modifier for DEX.
  abilityMod: number,

  // Equipped armor.
  armor: ArmorItem | undefined,

  // Equipped shield.
  shield: ShieldItem | undefined,

  // Total AC.
  value: number,
}

export function getACDetails(character: Character): ACDetails {
  const abilityMod = getAbilityScoreDetails(character.abilityScores).armorClassMod

  // Armor.
  const body = getEquippedItem(character, 'body')
  const armor = body && isArmor(body) ? body : undefined

  // Shield.
  const offHand = getEquippedItem(character, 'offHand')
  const shield = offHand && isShield(offHand) ? offHand : undefined

  // FIXME: Other equipped items can affect AC?

  // Compute AC.
  const value = (
    10
    + abilityMod
    + (armor?.armor.acMod || 0)
    + (shield?.shield.acMod || 0)
  )

  return { abilityMod, armor, shield, value }
}
