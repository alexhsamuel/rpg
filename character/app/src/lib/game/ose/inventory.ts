import type { Dice, Item, MissileRange } from './items'
import { STANDARD_ITEMS, isMissileWeapon, isMeleeWeapon, type EquipKind } from './items'
import type { Character } from './character'
import { getAbilityScoreDetails } from './abilityScores'

export type InventoryItem = {
  // Either an item, or the name of a standard item.
  item: string | Item
  // Default quantity is 1.
  quantity?: number,
  // Whether this is party loot; default is false.
  loot?: boolean,
}

export type Inventory = InventoryItem[]

export type InventoryDetailsItem = {
  item: Item,
  quantity: number,
  loot: boolean,
}

export type InventoryDetails = InventoryDetailsItem[]

export function getInventoryDetailsItem(inv: InventoryItem): InventoryDetailsItem {
  let {quantity, item, loot} = inv
  loot = !!loot

  if (quantity === undefined)
    quantity = 1

  // The `item` field may be the name of a standard item, or a full item obj.
  if (typeof(item) === 'string')
    // If we can't find the standard item, mock up a simple one.
    return {quantity, loot, item: STANDARD_ITEMS[item] || {name: item, cost: undefined}}

  else
    return {quantity, loot, item}
}

export function getInventoryDetails(inv: Inventory): InventoryDetails {
  return inv.map(getInventoryDetailsItem).filter(({quantity}) => quantity > 0)
}

export function getInventoryItem(inventory: Inventory, itemName: string): InventoryDetailsItem | undefined {
  return getInventoryDetails(inventory).find(({item}) => item.name === itemName)
}

export function inventoryAdd(character: Character, item: InventoryItem): Character {
  const inventory = character.inventory
  const isStandard = typeof(item.item) === 'string'
  let newInventory

  const index = inventory.findIndex(i => i.item === item.item)
  if (index !== -1) {
    // Coalesce standard items by updating quantity.
    newInventory = [...inventory]
    newInventory[index].quantity = (newInventory[index].quantity || 1) + (item.quantity || 1)
  }
  else {
    // New standard item, or nonstandard.  Add a new inventory item.
    newInventory = [item, ...inventory]
  }

  return {...character, inventory: newInventory}
}

export function inventoryIncrease(character: Character, itemName: string, quantity: number): Character {
  const inventory = character.inventory
  const index = inventory.findIndex(
    ({item}) => (typeof(item) === 'string' ? item : item.name) === itemName)
  if (index === undefined) {
    // FIXME: Error.
    console.error('no inventory item:', itemName)
    return character
  }

  const newQuantity = (inventory[index].quantity || 1) + quantity
  if (newQuantity < 0) {
    // FIXME: Error.
    console.error('insufficient quantity:', itemName, quantity)
    return character
  }

  let newInventory = [...inventory]
  if (newQuantity === 0)
    // FIXME: Unequip.
    newInventory.splice(index, 1)
  else if (newQuantity === 1)
    delete newInventory[index].quantity
  else
    newInventory[index].quantity = newQuantity
  return {...character, inventory: newInventory}
}

export function setLoot(character: Character, index: number, loot: boolean): Character {
  if (index < 0 || character.inventory.length <= index) {
    // FIXME: Error.
    console.error('no inventory index:', index)
    return character
  }

  const inventory = [...character.inventory]
  if (loot)
    inventory[index].loot = true
  else
    delete inventory[index].loot
  return {...character, inventory} 
}

//------------------------------------------------------------------------------

export type EquipLocation = 'mainHand' | 'offHand' | 'body'

export type Equipped = {
  mainHand?: string,
  offHand?: string | 'TWOHAND',
  body?: string,
}

export function getEquippedItem(character: Character, location: EquipLocation): Item | undefined {
  const itemName = character.equipped[location]
  return itemName && itemName !== 'TWOHAND' ? getInventoryItem(character.inventory, itemName)?.item : undefined  
}

export type EquippedDetails = {
  item: Item,
  twoHand: boolean,
  melee?: {
    attackMod: number,
    damage: Dice,
  },
  missile?: {
    attackMod: number,
    damage: Dice,
    range: MissileRange,
  },
}

export function getEquippedDetails(character: Character, location: EquipLocation): EquippedDetails | undefined {
  if (!location)
    return undefined

  const item = getEquippedItem(character, location)
  if (!item)
    return undefined

  let details: EquippedDetails = {
    item,
    twoHand: location === 'mainHand' && character.equipped.offHand === 'TWOHAND',
    melee: undefined,
    missile: undefined,
  }

  if (location == 'mainHand' && isMeleeWeapon(item)) {
    const scores = getAbilityScoreDetails(character.abilityScores)
    details.melee = {
      attackMod: scores.meleeMod,
      damage: item.weapon.damage + '+' + scores.meleeMod,  // FIXME: Proper dice.
    }
  }

  if (location === 'mainHand' && isMissileWeapon(item)) {
    const scores = getAbilityScoreDetails(character.abilityScores)
    details.missile = {
      attackMod: scores.missileMod,
      damage: item.weapon.damage,
      range: item.weapon.missileRange!,
    }
  }

  return details
}

function canEquip(location: EquipLocation, equipKind: EquipKind) {
  return (
    location === 'mainHand' ? equipKind === 'hand' || equipKind === 'twoHand'
    : location === 'offHand' ? equipKind === 'hand'
    : location === equipKind
  )
}

/// Returns all inventory items that can be equipped at `location`.
export function getEquippable(character: Character, location: EquipLocation): Item[] {
  return getInventoryDetails(character.inventory).map(({item}) => item).filter(item => item.equipKind && canEquip(location, item.equipKind))
}

export function unequip(character: Character, location: EquipLocation): Character {
  let equipped = {...character.equipped}

  if (equipped[location]) {
    delete equipped[location]
    if (location === 'mainHand' && equipped.offHand === 'TWOHAND')
      delete equipped.offHand
    if (location === 'offHand' && equipped.offHand === 'TWOHAND')
      delete equipped.mainHand
    return {...character, equipped}
  }
  else {
    // FIXME: Error.
    console.error(`nothing equipped: ${location}`)
    return character
  }
}

export function equip(character: Character, itemName: string, location: EquipLocation): Character {
  const equipped = {...character.equipped}

  const inv = getInventoryItem(character.inventory, itemName)
  if (inv === undefined) {
    // FIXME: Error.
    console.error(`can't equip unknown item ${itemName}`)
    return character
  }

  const item = inv.item

  // Check if item can be equipped.
  if (!(item.equipKind && canEquip(location, item.equipKind))) {
    // FIXME: Error.
    console.error(`can't equip ${item.name} as ${location}`)
    return character
  }

  // Unequip first.
  if (location === 'mainHand' && equipped.offHand === 'TWOHAND')
    delete equipped.offHand
  if (location === 'offHand' && equipped.offHand === 'TWOHAND')
    delete equipped.mainHand

  // Equip.
  equipped[location] = itemName
  if (item?.equipKind === 'twoHand')
    equipped.offHand = 'TWOHAND'

  return {...character, equipped}
}

