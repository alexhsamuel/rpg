import type { Character } from './character'

export const COIN_TYPES = ['platinum', 'gold', 'electrum', 'silver', 'copper'] as const
export type CoinType = typeof COIN_TYPES[number]

export type Coins = {
  [coinType in CoinType]: number
}

type Value = number  // value in gp; use 0.01 for cp 
type _Cost = {value: Value, quantity: number} 
export type Cost = _Cost | number

export function formatValue(value: Value): string {
  if (Math.round(value) === value)
    return `${value} GP`
  value *= 10
  if (Math.round(value) === value)
    return `${value} SP`
  else
    return `${value * 10} CP`
}

export function formatCost(cost: Cost): string {
  let value, quantity = 1
  if (typeof(cost) === 'number')
    value = cost
  else {
    value = cost.value
    quantity = cost.quantity
  }
  let f = formatValue(value)
  return quantity === 1 ? f : `${f} / ${quantity}`
}

function normalizeCost(cost: Cost): _Cost {
  return typeof(cost) === 'number' ? {value: cost, quantity: 1} : cost
}

export function getCoinAbbreviation(coinType: CoinType) {
  return coinType.substring(0, 1).toUpperCase() + 'P' 
}

export function addCoins(character: Character, coinType: CoinType, value: number): Character {
  const current = character.coins[coinType]
  if (current + value < 0) {
    // FIXME: Error.
    console.error(`can't add ${value} coins to current ${current} ${getCoinAbbreviation(coinType)}`)
    return character
  }

  return {
    ...character,
    coins: {...character.coins, [coinType]: current + value}
  }
}
