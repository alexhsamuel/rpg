export const ABILITY_SCORE_NAMES = ['strength', 'intelligence', 'wisdom', 'dexterity', 'constitution', 'charisma'] as const
type AbilityScoreNames = typeof ABILITY_SCORE_NAMES[number]

export type AbilityScores = {
  [name in AbilityScoreNames]: number
}

//------------------------------------------------------------------------------

export type AbilityScoreDetails = {
  meleeMod: number,
  openDoors: number,
  magicSaveMod: number,
  armorClassMod: number,
  missileMod: number,
  initiativeMod: number,
  hitPointMod: number,
  npcReactionMod: number,
  maxRetainers: number,
  retainerLoyalty: number,
  // FIXME: prime requisite modifier
}

/// Returns a function to look up a modifier by ability score.  `number` is a 
/// 7-element array with values for ability scores 3, 4-5, 6-8, 9-12, 13-15,
/// 16-17, and 18.
function abilityScoreLookup(ability: AbilityScoreNames, table: number[]): (abilityScores: AbilityScores) => number {
  return abilityScores => {
    const val = abilityScores[ability]
    if (val < 3 || 18 < val)
      throw new Error('invalid ability score')
    return (
      val == 3 ? table[0]
      : val < 6 ? table[1]
      : val < 9 ? table[2]
      : val < 13 ? table[3]
      : val < 16 ? table[4]
      : val < 18 ? table[5]
      : table[6]
    )
  }
}

const STANDARD_MODS = [-3, -2, -1, 0, 1, 2, 3]
const MELEE_MOD = abilityScoreLookup('strength', STANDARD_MODS)
const OPEN_DOORS = abilityScoreLookup('strength', [1, 1, 1, 2, 3, 4, 5])
const MAGIC_SAVE_MOD = abilityScoreLookup('intelligence', STANDARD_MODS)
const ARMOR_CLASS_MOD = abilityScoreLookup('dexterity', STANDARD_MODS)
const MISSILE_MOD = abilityScoreLookup('dexterity', STANDARD_MODS)
const INITIATIVE_MOD = abilityScoreLookup('dexterity', [-2, -1, -1, 0, 1, 1, 2])
const HIT_POINT_MOD = abilityScoreLookup('constitution', STANDARD_MODS)
const NPC_REACTION_MOD = abilityScoreLookup('charisma', [-2, -1, -1, 0, 1, 1, 2])
const MAX_RETAINTERS = abilityScoreLookup('charisma', [1, 2, 3, 4, 5, 6, 7])
const RETAINER_LOYALTY = abilityScoreLookup('charisma', [4, 5, 6, 7, 8, 9, 10])


export function getAbilityScoreDetails(abilityScores: AbilityScores): AbilityScoreDetails {
  return {
    meleeMod: MELEE_MOD(abilityScores),
    openDoors: OPEN_DOORS(abilityScores),
    magicSaveMod: MAGIC_SAVE_MOD(abilityScores),
    armorClassMod: ARMOR_CLASS_MOD(abilityScores),
    missileMod: MISSILE_MOD(abilityScores),
    initiativeMod: INITIATIVE_MOD(abilityScores),
    hitPointMod: HIT_POINT_MOD(abilityScores),
    npcReactionMod: NPC_REACTION_MOD(abilityScores),
    maxRetainers: MAX_RETAINTERS(abilityScores),
    retainerLoyalty: RETAINER_LOYALTY(abilityScores),
  }
}

