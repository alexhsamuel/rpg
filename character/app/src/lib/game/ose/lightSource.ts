import type { Character } from "./character"
import { HAND_EQUIP_LOCATIONS, getInventoryItem, type EquipLocation } from "./inventory"
import { defined } from "./lib"

export type LightSource = {
  name: string,
  range: number,
  lit: boolean,
}

export function getLightSource(character: Character, location: EquipLocation): LightSource | undefined {
  const equipped = character.equipped[location]
  if (equipped) {
    const item = getInventoryItem(character.inventory, equipped)?.item
    if (item && item.light) {
      const lit =
        item.name === 'lantern'
        ? getInventoryItem(character.inventory, 'oil, flask') !== undefined
        : true
      return {name: item.name, lit, ...item.light}
    }
  }
}

export function getLightSources(character: Character): LightSource[] {
  return HAND_EQUIP_LOCATIONS.map(l => getLightSource(character, l)).filter(defined)
}

