import type { ByName, Modify } from './lib'
import { byName } from './lib'
import type { Cost } from './coins'

export type Dice = string  // for now

export type EquipKind = 'hand' | 'twoHand' | 'body'
export type WeaponQuality = 'melee' | 'missile' | 'two-handed' | 'slow' | 'blunt' | 'reload' | 'brace' | 'charge' | 'splash'
export type MissileRange = {short: number, medium: number, long: number}

export type Weapon = {
  damage: Dice,
  qualities: WeaponQuality[],
  missileRange?: MissileRange,
}

export type Armor = {
  acMod: number,
}

export type Shield = {
  acMod: number,
}

export type Item = {
  name: string,
  cost?: Cost,
  weight?: number,
  equipKind?: EquipKind,
  weapon?: Weapon,
  armor?: Armor,
  shield?: Shield,
  light?: {
    range: number,
    duration: number,  // [turns]
  },
}

export type WeaponItem = Modify<Item, {weapon: Weapon}>
export type ArmorItem = Modify<Item, {armor: Armor}>
export type ShieldItem = Modify<Item, {shield: Shield}>

type Gear = Item

const STANDARD_GEAR: ByName<Gear> = byName([
  {
    name: 'backback',
    cost: 5,
  },
  {
    name: 'crowbar',
    cost: 10,
  },
  {
    name: 'garlic',
    cost: 5,
  },
  {
    name: 'grappling hook',
    cost: 25,
  },
  {
    name: 'small hammer',
    cost: 2
  },
  {
    name: 'holy symbol',
    cost: 25,
    equipKind: 'hand',
  },
  {
    name: 'holy water',
    cost: 25,
  },
  {
    name: 'iron spike',
    cost: {value: 1, quantity: 12},
  },
  {
    name: 'lantern',
    equipKind: 'hand',
    cost: 10,
    light: {range: 30, duration: 24},
  },
  {
    name: 'steel mirror',
    cost: 5,
  },
  {
    name: 'oil, flask',
    cost: 2,
    equipKind: 'hand',
    weapon: {
      damage: '1d8',
      qualities: ['missile', 'splash'] as WeaponQuality[],
      missileRange: {short: 10, medium: 30, long: 50},
    },
  },
  {
    name: 'pole, 10\' wooden',
    cost: 1,
    equipKind: 'hand',
  },
  {
    name: 'iron ration',
    cost: {value: 15, quantity: 7},
  },
  {
    name: 'standard ration',
    cost: {value: 5, quantity: 7},
  },
  {
    name: 'rope, 50\'',
    cost: 1,
  },
  {
    name: 'large sack',
    cost: 2,
  },
  {
    name: 'small sack',
    cost: 1,
  },
  {
    name: 'stake',
    cost: {value: 3, quantity: 3},
  },
  {
    name: 'thieve\'s tools',
    cost: 25,
  },
  {
    name: 'tinder box',
    cost: 3,
  },
  {
    name: 'torch',
    equipKind: 'hand',
    cost: {value: 1, quantity: 6},
    light: {range: 30, duration: 6},
    weapon: {
      damage: '1d4',
      qualities: ['melee'] as WeaponQuality[],
    },
  },
  {
    name: 'waterskin',
    cost: 1,
  },
  {
    name: 'wine, 2 pints',
    cost: 1,
  },
  {
    name: 'wolfsbane, bunch',
    cost: 10,
  },
])

/// Checks and asserts that `item` is a weapon.
/// If `weaponQuality`, true only if the weapon has this quality.
export function isWeapon(item: Item): item is WeaponItem {
  return !!item.weapon
}

export function isMeleeWeapon(item: Item, weaponQuality?: WeaponQuality): item is WeaponItem {
  return !!item.weapon && item.weapon.qualities.includes('melee')
}

export function isMissileWeapon(item: Item, weaponQuality?: WeaponQuality): item is WeaponItem {
  return !!item.weapon && item.weapon.qualities.includes('missile')
}

const STANDARD_WEAPONS: ByName<Item> = byName([
  {
    name: 'battle axe',
    cost: 7,
    weight: 50,
    weapon: {
      damage: '1d8',
      qualities: ['melee', 'slow', 'two-handed'] as WeaponQuality[],
    },
  },
  {
    name: 'club',
    cost: 3,
    weight: 50,
    weapon: {
      damage: '1d4',
      qualities: ['blunt', 'melee'] as WeaponQuality[],
    }
  },
  {
    name: 'crossbow',
    cost: 30,
    weight: 50,
    weapon: {
      damage: '1d6',
      qualities: ['missile', 'reload', 'slow', 'two-handed'] as WeaponQuality[],
      missileRange: {short: 80, medium: 160, long: 240},
    },
  },
  {
    name: 'dagger',
    cost: 3,
    weight: 10,
    weapon: {
      damage: '1d4',
      qualities: ['melee', 'missile'] as WeaponQuality[],
      missileRange: {short: 10, medium: 20, long: 30},
    },
  },
  {
    name: 'hand axe',
    cost: 4,
    weight: 30,
    weapon: {
      damage: '1d6',
      qualities: ['melee', 'missile'] as WeaponQuality[],
      missileRange: {short: 10, medium: 20, long: 30},
    },
  },
  {
    name: 'javelin',
    cost: 1,
    weight: 20,
    weapon: {
      damage: '1d4',
      qualities: ['missile'] as WeaponQuality[],
      missileRange: {short: 30, medium: 60, long: 90},
    },
  },
  {
    name: 'lance',
    cost: 5,
    weight: 120,
    weapon: {
      damage: '1d6',
      qualities: ['charge', 'melee'] as WeaponQuality[],
    },
  },
  {
    name: 'long bow',
    cost: 40,
    weight: 30,
    weapon: {
      damage: '1d6',
      qualities: ['missile', 'two-handed'] as WeaponQuality[],
      missileRange: {short: 70, medium: 140, long: 210},
    },
  },
  {
    name: 'mace',
    cost: 5,
    weight: 30,
    weapon: {
      damage: '1d6',
      qualities: ['blunt', 'melee'] as WeaponQuality[],
    },
  },
  {
    name: 'pole arm',
    cost: 7,
    weight: 150,
    weapon: {
      damage: '1d10',
      qualities: ['brace', 'melee', 'slow', 'two-handed'] as WeaponQuality[],
    },
  },
  {
    name: 'short bow',
    cost: 25,
    weight: 30,
    weapon: {
      damage: '1d6',
      qualities: ['missile', 'two-handed'] as WeaponQuality[],
      missileRange: {short: 50, medium: 100, long: 150},
    },
  },
  {
    name: 'short sword',
    cost: 7,
    weight: 30,
    weapon: {
      damage: '1d6',
      qualities: ['melee'] as WeaponQuality[],
    },
  },
  {
    name: 'silver dagger',
    cost: 30,
    weight: 10,
    weapon: {
      damage: '1d4',
      qualities: ['melee', 'missile'] as WeaponQuality[],
      missileRange: {short: 10, medium: 20, long: 30},
    },
  },
  {
    name: 'sling',
    cost: 2,
    weight: 20,
    weapon: {
      damage: '1d4',
      qualities: ['blunt', 'missile'] as WeaponQuality[],
      missileRange: {short: 40, medium: 80, long: 160},
    },
  },
  {
    name: 'spear',
    cost: 3,
    weight: 30,
    weapon: {
      damage: '1d6',
      qualities: ['brace', 'melee', 'missile'] as WeaponQuality[],
      missileRange: { short: 20, medium: 40, long: 60},
    },
  },
  {
    name: 'staff',
    cost: 2,
    weapon: {
      weight: 40,
      damage: '1d4',
      qualities: ['blunt', 'melee', 'slow', 'two-handed'] as WeaponQuality[],
    },
  },
  {
    name: 'sword',
    cost: 10,
    weight: 60,
    weapon: {
      damage: '1d8',
      qualities: ['melee'] as WeaponQuality[],
    },
  },
  {
    name: 'two-handed sword',
    cost: 15,
    weight: 150,
    weapon: {
      damage: '1d10',
      qualities: ['melee', 'slow', 'two-handed'] as WeaponQuality[],
    },
  },
  {
    name: 'war hammer',
    cost: 5,
    weight: 30,
    weapon: {
      damage: '1d6',
      qualities: ['blunt', 'melee'] as WeaponQuality[],
    },
  },
].map(w => ({
  ...w, 
  equipKind: w.weapon.qualities.includes('two-handed') ? 'twoHand' : 'hand'
})))

/// Checks and asserts that `item` is an armor.
export function isArmor(item: Item): item is ArmorItem {
  return !!item.armor
}

const STANDARD_ARMORS: ByName<ArmorItem> = byName([
  {
    name: 'leather armor',
    cost: 20,
    weight: 200,
    armor: {
      acMod: 2,
    },
  },
  {
    name: 'chainmail',
    armorClassAdj: 4,
    cost: 40,
    weight: 400,
    armor: {
      acMod: 4,
    },
  },
  {
    name: 'plate mail',
    cost: 60,
    weight: 500,
    armor: {
      acMod: 6,
    },
  },
].map(a => ({...a, equipKind: 'body'})))

const STANDARD_SHIELDS: ByName<ShieldItem> = byName([
  {
    name: 'shield',
    equipLocation: 'hand',
    cost: 10,
    weight: 100,
    shield: {
      acMod: 1,
    }
  },
].map(a => ({...a, equipKind: 'hand'})))

export function isShield(item: Item): item is ShieldItem {
  return !!item.shield
}

export const STANDARD_ITEMS: ByName<Gear> = {
  ...STANDARD_GEAR,
  ...STANDARD_ARMORS,
  ...STANDARD_WEAPONS,
  ...STANDARD_SHIELDS,
}

