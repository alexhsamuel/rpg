export function adjustment(value: number) {
  return value >= 0 ? '+' + value.toString() : '–' + (-value).toString()
}

