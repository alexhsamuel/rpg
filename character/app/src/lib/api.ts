import type { Character, CharacterAction } from '$lib/game/ose/character'

type Version = string
export type VersionedCharacter = { version: Version, character: Character }

function removeUndefinedValues<V>(obj: { [k: string]: V | undefined }): { [k: string] : V } {
  return Object.fromEntries(
    Object.entries(obj)
    .filter(([k, v]) => v !== undefined) as [string, V][]
  )
}

function path(parts: string[], params: { [key: string]: string | undefined }) {
  let path = '/api/' + parts.join('/') 
  const cleanParams = removeUndefinedValues(params)
  if (cleanParams)
    path += '?' + new URLSearchParams(cleanParams)
  return path
}

export async function getCharacter(fetch: any, name: string, after: Version | 'next' | undefined = undefined): Promise<VersionedCharacter> {
  return await (await fetch(
    path(['characters', name], { after })
  )).json()
}

export async function doAction(version: Version, name: string, action: CharacterAction): Promise<VersionedCharacter> {
  const rsp = await fetch(
    path(['characters', name], { version }),
    {
      method: 'PUT',
      headers: {
        'content-type': 'application/json',
      },
      body: JSON.stringify(action),
    }
  )
  return await rsp.json()
}
