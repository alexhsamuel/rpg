import type { VersionedCharacter } from '$lib/api'
import { getCharacter } from '$lib/api'

const NAME = 'eucomis'

type Data = VersionedCharacter & {
  name: string,
}

export async function load({ params, fetch }): Promise<Data> {
  const name = params.name
  const { version, character } = await getCharacter(fetch, name)
  return {
    name,
    version,
    character,
  }
}

