# Features

- tracks hit points
- tracks worn armor and weapons/items wielded in each hand
- computes AC from armor, shield, +DEX
- tracks wielded light sources (torche, lantern)
- tracks coins


# Todo

### v0.1.0

- [x] track equip
  - [x] action
  - [x] top-level indicator
- [x] AC
  - [x] calc
  - [x] control
- [x] track light
  - [x] lantern + oil rules
  - [x] top-level indicator
- [x] bonuses / dmg for equipped weapons
- [x] modal HP control
- [x] modal equip controls
- [x] modal armor control
- [x] inventory controls
  - [x] deplete
  - [x] delete item
  - [x] add standard item
  - [x] add custom item
  - [x] loot marker
- [x] encumbrance
  - [x] calc
  - [x] box
  - [x] movement speed
- [x] coin controls
  - [x] action
- [x] icons for controls
- [x] saving throws
- [ ] tweak layout for mobile

### v0.2.0

- [ ] log
- [ ] notes
- [ ] errors
- [ ] ammo
- [ ] XP
  - [ ] action
  - [ ] primary ability bonus

### v0.3.0

- [ ] party overview
  - [ ] party object
  - [ ] route
  - [ ] nav
  - [ ] party loot summary
- [ ] marching order
- [ ] custom items in store
- [ ] inotify on store


# Cleanup

- CSS vars for colors
- CSS vars for spacing lengths


# Features

State-mutating operations on a character:

- hit points
- statuses (prone, poisoned, ?)
- inventory
    - add, remove items
    - deplete (torches, spikes)
    - coins
    - shared loot for later division
- encumbrance
- movement
- wielding: armor, what's in each hand
    - AC
    - weapon hit adj / damage 
- light sources
- spells

Adventure-level state:

- marching order
- time tracking
- initiative & turn tracking

