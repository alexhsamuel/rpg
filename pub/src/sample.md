# A Sample

This is a sample Markdown file.  Let's try some inline markup: _italics_ , **bold**, and ~~strikethrough~~.

<span color="green">This might be in green.</span>

## A table

| Left Column | Right Column |
|:------------|-------------:|
| banana | yellow |
| apple | red |
| plum | purple |

## Fenced code

```py
def greet(name):
    message = f"Hello, {name}!"
    print(message)

```

