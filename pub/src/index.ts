import marked from 'marked'
import fs from 'fs'
import { argv } from 'node:process'
import jsdom from 'jsdom'

function parse(markdown: string): jsdom.JSDOM {
    const html = marked.marked.parse(markdown, {mangle: false, headerIds: false})
    const dom = new jsdom.JSDOM(html)
    return dom
}

function parse_file(path: string): jsdom.JSDOM {
    const markdown = fs.readFileSync(path, 'utf8')
    return parse(markdown)
}

console.log(parse_file(argv[2]).serialize())

